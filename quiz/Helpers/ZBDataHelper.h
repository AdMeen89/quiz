//
//  ZBDataHelper.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ZBTheme;

typedef void(^ZBEmptyBlock)(void);
typedef NS_ENUM(NSUInteger, ZBDataHelperType)
{
    ZBDataHelperTypeThemes,
    ZBDataHelperTypeAnswers,
    ZBDataHelperTypeQuestions,
};

@protocol ZBDataHelperDelegate <NSObject>

- (void)dataHelperFinishType:(ZBDataHelperType)type withError:(NSError *)error;

@end

@interface ZBDataHelper : NSObject

@property (weak, nonatomic) id<ZBDataHelperDelegate> delegate;

- (void)loadAndSaveThemes;
- (void)loadAndSaveQuestionsWithAnswers;

@end
