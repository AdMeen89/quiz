//
//  ZBDataHelper.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBDataHelper.h"

#import "ZBTheme.h"
#import "ZBQuestion.h"
#import "ZBAnswer.h"

#import "ZBAnswerObject.h"
#import "ZBThemeObject.h"
#import "ZBQuestionObject.h"

@implementation ZBDataHelper

- (void)loadAndSaveThemes
{
    PFQuery *query = [ZBThemeObject query];
    
    id completedBlock = ^(NSArray * _Nullable themes, NSError * _Nullable error)
    {
        if (error)
        {
            if ([self.delegate respondsToSelector:@selector(dataHelperFinishType:withError:)])
            {
                [self.delegate dataHelperFinishType:ZBDataHelperTypeThemes withError:error];
            }
            
            return;
        }
        
        if ([themes count])
        {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            
            for (ZBThemeObject *theme in themes)
            {
                ZBTheme *obj = (ZBTheme *)[theme asRealmObject];
                [realm addOrUpdateObject:obj];
            }
            
            [realm commitWriteTransaction];
            
        }
        
        if ([self.delegate respondsToSelector:@selector(dataHelperFinishType:withError:)])
        {
            [self.delegate dataHelperFinishType:ZBDataHelperTypeThemes withError:error];
        }
    };
    
    [query findObjectsInBackgroundWithBlock:completedBlock];
}

- (void)loadAndSaveAnswerWithQuestion:(ZBQuestionObject *)question
{
    id completedBlock = ^(NSArray * _Nullable answers, NSError * _Nullable error)
    {
        if (error)
        {
            if ([self.delegate respondsToSelector:@selector(dataHelperFinishType:withError:)])
            {
                [self.delegate dataHelperFinishType:ZBDataHelperTypeAnswers withError:error];
            }
            
            return;
        }
        
        if ([answers count])
        {
            RLMRealm *realm = [RLMRealm defaultRealm];
            
            if (![realm inWriteTransaction])
            {
                [realm beginWriteTransaction];
            }
            
            for (ZBAnswerObject *answer in answers)
            {
                ZBAnswer *obj = (ZBAnswer *)[answer asRealmObject];
                obj.question = [ZBQuestion objectForPrimaryKey:question.objectId];
                [realm addOrUpdateObject:obj];
            }
            
            [realm commitWriteTransaction];
        }
        
        if ([self.delegate respondsToSelector:@selector(dataHelperFinishType:withError:)])
        {
            [self.delegate dataHelperFinishType:ZBDataHelperTypeAnswers withError:error];
        }
    };

    id fetchedBlock= ^(ZBQuestionObject * _Nullable object, NSError * _Nullable error)
    {
        PFRelation *answers = question[@"answers"];
        [[answers query] findObjectsInBackgroundWithBlock:completedBlock];
    };
    
    [question fetchIfNeededInBackgroundWithBlock:fetchedBlock];
}

- (void)loadAndSaveQuestionsWithAnswers
{
    PFQuery *query = [ZBQuestionObject query];
    
    id completedBlock = ^(NSArray * _Nullable questions, NSError * _Nullable error)
    {
        if (error)
        {
            if ([self.delegate respondsToSelector:@selector(dataHelperFinishType:withError:)])
            {
                [self.delegate dataHelperFinishType:ZBDataHelperTypeQuestions withError:error];
            }
            
            return;
        }
        
        if ([questions count])
        {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            
            for (ZBQuestionObject *question in questions)
            {
                ZBQuestion *obj = (ZBQuestion *)[question asRealmObject];
                [realm addOrUpdateObject:obj];
                [self loadAndSaveAnswerWithQuestion:question];
            }
            
            [realm commitWriteTransaction];
            
        }
        
        if ([self.delegate respondsToSelector:@selector(dataHelperFinishType:withError:)])
        {
            [self.delegate dataHelperFinishType:ZBDataHelperTypeQuestions withError:error];
        }
    };
    
    [query findObjectsInBackgroundWithBlock:completedBlock];}


@end
