//
//  ZBQuestion.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <Realm/Realm.h>
#import "ZBTheme.h"

@interface ZBQuestion : RLMObject

@property NSString *objectId;
@property NSDate *createdAt;
@property NSDate *updatedAt;

@property NSString *text;
@property NSNumber<RLMInt> *level;
@property ZBTheme *theme;

@property (readonly) NSArray *answers;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<ZBQuestion>
RLM_ARRAY_TYPE(ZBQuestion)
