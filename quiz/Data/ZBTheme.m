//
//  ZBTheme.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBTheme.h"
#import "ZBQuestion.h"

@implementation ZBTheme

// Specify default values for properties

+ (NSString *)primaryKey
{
    return @"objectId";
}

- (NSArray *)questions
{
    return [self linkingObjectsOfClass:[[ZBQuestion class] description] forProperty:@"theme"];
}

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
