//
//  ZBTags.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <Realm/Realm.h>

@interface ZBTags : RLMObject

@property NSString *tag;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<ZBTags>
RLM_ARRAY_TYPE(ZBTags)
