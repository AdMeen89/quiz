//
//  ZBQuestion.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBQuestion.h"
#import "ZBAnswer.h"

@implementation ZBQuestion

+ (NSString *)primaryKey
{
    return @"objectId";
}

- (NSArray *)answers
{
    return [self linkingObjectsOfClass:[[ZBAnswer class] description] forProperty:@"question"];
}

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
