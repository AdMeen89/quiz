//
//  ZBAnswer.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <Realm/Realm.h>
#import "ZBQuestion.h"

@interface ZBAnswer : RLMObject

@property NSString *objectId;
@property NSDate *createdAt;
@property NSDate *updatedAt;

@property NSString *text;
@property NSNumber<RLMBool> *correct;
@property ZBQuestion *question;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<ZBAnswer>
RLM_ARRAY_TYPE(ZBAnswer)
