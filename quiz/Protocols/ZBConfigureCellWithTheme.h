//
//  ZBConfigureCellWithTheme.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZBTheme;

@protocol ZBConfigureCellWithTheme <NSObject>

- (void)configureCell:(UITableViewCell *)cell withTheme:(ZBTheme *)theme userData:(id)userData;

@end
