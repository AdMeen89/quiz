//
//  ZBParseToRealmProtocol.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RLMObject;

@protocol ZBParseToRealmProtocol <NSObject>

- (RLMObject *)asRealmObject;

@end
