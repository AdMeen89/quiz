//
//  ZBAnswerObject.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <Parse/Parse.h>
#import "ZBParseToRealmProtocol.h"

@interface ZBAnswerObject : PFObject <PFSubclassing, ZBParseToRealmProtocol>

@property (copy, nonatomic) NSString *text;
@property (strong, nonatomic) NSNumber *correct;

@end
