//
//  ZBThemeObject.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBThemeObject.h"
#import "ZBTheme.h"

@implementation ZBThemeObject

@dynamic name, description, tags, image;

#pragma mark - PFSubclassing

+ (void)load
{
    [self registerSubclass];
}

+ (NSString *)parseClassName
{
    return @"theme";
}

#pragma mark - public

- (ZBTheme *)asRealmObject
{
    ZBTheme *theme = [ZBTheme new];
    theme.name = self.name;
    
    if (!self.image.isDataAvailable)
    {
        NSURL *url = [NSURL URLWithString:self.image.url];
        theme.image = [NSData dataWithContentsOfURL:url];
    }
    else
    {
        theme.image = [self.image getData];
    }
    
    theme.descr = self.description;
    theme.objectId = self.objectId;
    theme.createdAt = self.createdAt;
    theme.updatedAt = self.updatedAt;
    
    return theme;
}

@end
