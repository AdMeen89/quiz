//
//  ZBAnswerObject.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBAnswerObject.h"
#import "ZBAnswer.h"

@implementation ZBAnswerObject

@dynamic correct, text;

#pragma mark - PFSubclassing

+ (void)load
{
    [self registerSubclass];
}

+ (NSString *)parseClassName
{
    return @"answer";
}

#pragma mark - public

- (ZBAnswer *)asRealmObject
{
    ZBAnswer *answer = [ZBAnswer new];
    
    answer.objectId = self.objectId;
    answer.createdAt = self.createdAt;
    answer.updatedAt = self.updatedAt;
    
    answer.correct = self.correct;
    answer.text = self.text;
    
    return answer;
}

@end
