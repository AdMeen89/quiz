//
//  ZBQuestionObject.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <Parse/Parse.h>
#import "ZBParseToRealmProtocol.h"

@class ZBQuestion;

@interface ZBQuestionObject : PFObject <PFSubclassing, ZBParseToRealmProtocol>

@property (copy, nonatomic) NSString *text;
@property (strong, nonatomic) NSNumber *level;
@property (copy, nonatomic) NSString *themeId;

@end
