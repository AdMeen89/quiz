//
//  ZBThemeObject.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <Parse/Parse.h>
#import "ZBParseToRealmProtocol.h"

@class ZBTheme;

@interface ZBThemeObject : PFObject <PFSubclassing, ZBParseToRealmProtocol>

@property (copy, nonatomic) NSString *name;
@property (strong, nonatomic) PFFile *image;
@property (copy, nonatomic) NSString *tags;

@end
