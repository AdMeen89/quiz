//
//  ZBQuestionObject.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBQuestionObject.h"
#import "ZBQuestion.h"

@implementation ZBQuestionObject

@dynamic text, level, themeId;

#pragma mark - PFSubclassing

+ (void)load
{
    [self registerSubclass];
}

+ (NSString *)parseClassName
{
    return @"question";
}

#pragma mark - public

- (ZBQuestion *)asRealmObject
{
    ZBQuestion *question = [ZBQuestion new];
    
    question.objectId = self.objectId;
    question.createdAt = self.createdAt;
    question.updatedAt = self.updatedAt;
    
    question.text = self.text;
    question.level = self.level;
    question.theme = [ZBTheme objectForPrimaryKey:self.themeId];
    
    return question;
}

@end
