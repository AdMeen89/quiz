//
//  AppDelegate.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

