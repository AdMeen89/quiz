//
//  ZBLoadingViewController.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBLoadingViewController.h"
#import "ZBDataHelper.h"
#import "Macroses.h"

@interface ZBLoadingViewController () <ZBDataHelperDelegate>

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) ZBDataHelper *dataHelper;
@property (nonatomic) NSUInteger finishedCount;

@end

@implementation ZBLoadingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setup];
    [self loadData];
}

#pragma mark - other

- (void)setup
{
    self.infoLabel.text = @"#loading";
    self.finishedCount = 0;
}

- (void)loadData
{
    [self.dataHelper loadAndSaveThemes];
    [self.dataHelper loadAndSaveQuestionsWithAnswers];
}

#pragma mark - ZBDataHelperDelegate

-(void)dataHelperFinishType:(ZBDataHelperType)type withError:(NSError *)error
{
    if (!error && self.finishedCount == 3)
    {
        ASYNC({[self performSegueWithIdentifier:@"start" sender:nil];});
        return;
    }
    
    if (!error)
    {
        self.finishedCount ++;
    }
}

#pragma mark - getters

- (ZBDataHelper *)dataHelper
{
    if (!_dataHelper)
    {
        _dataHelper = [ZBDataHelper new];
        _dataHelper.delegate = self;
    }
    
    return _dataHelper;
}

@end
