//
//  ZBThemesViewController.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBThemesViewController.h"
#import "ZBTheme.h"
#import "ZBConfigureCellWithTheme.h"
#import "ZBQuestionsViewController.h"

@interface ZBThemesViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RLMResults *results;

@end

@implementation ZBThemesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}

#pragma mark - tableview

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"theme cell" forIndexPath:indexPath];
    
    if ([cell conformsToProtocol:@protocol(ZBConfigureCellWithTheme)])
    {
        ZBTheme *theme = [self.results objectAtIndex:indexPath.row];
        [((id<ZBConfigureCellWithTheme>)cell) configureCell:cell withTheme:theme userData:nil];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZBTheme *theme = [self.results objectAtIndex:indexPath.row];
    ZBQuestionsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[[ZBQuestionsViewController class] description]];
    vc.themeId = theme.objectId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - getters

- (RLMResults *)results
{
    if (!_results)
    {
        _results = [[ZBTheme allObjects] sortedResultsUsingProperty:@"name" ascending:YES];
    }
    
    return _results;
}

@end
