//
//  ZBQuestionsViewController.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBQuestionsViewController.h"
#import "ZBQuestion.h"
#import "ZBAnswer.h"

@interface ZBQuestionsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) RLMResults *results;
@property (weak, nonatomic) ZBQuestion *currentQuestion;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@end

@implementation ZBQuestionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.currentQuestion = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.questionLabel.text = self.currentQuestion.text;
    
    NSUInteger count = 0;
    
    for (UIButton *btn in self.buttons)
    {
        ZBAnswer *answer = [self.currentQuestion.answers objectAtIndex:count];
        NSString *questionString = [NSString stringWithFormat:@"%ld) %@", (long)count+1, answer.text];
        [btn setTitle:questionString forState:UIControlStateNormal];
        count++;
    }
}

#pragma mark - getters

- (RLMResults *)results
{
    if (!_results)
    {
        _results = [[ZBQuestion allObjects] sortedResultsUsingProperty:@"level" ascending:YES];
    }
    
    return _results;
}

- (ZBQuestion *)currentQuestion
{
    if (!_currentQuestion)
    {
        NSInteger index = arc4random()%self.results.count;
        _currentQuestion = [self.results objectAtIndex:index];
    }
    
    return _currentQuestion;
}

- (IBAction)buttonPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    ZBAnswer *answer = [self.currentQuestion.answers objectAtIndex:button.tag];
    
    if ([answer.correct boolValue])
    {
        NSLog(@"+");
    }
    else
    {
        NSLog(@"-");
    }
}


@end
