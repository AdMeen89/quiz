//
//  ZBThemeItemTableViewCell.m
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import "ZBThemeItemTableViewCell.h"
#import "ZBTheme.h"
#import <FXBlurView/FXBlurView.h>

@interface ZBThemeItemTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *tagsLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation ZBThemeItemTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)prepareForReuse
{
    self.nameLabel.text = @"";
    self.countLabel.text = @"0";
    self.iconImageView.hidden = YES;
    self.tagsLabel.text = @"";
    self.scoreLabel.text = @"0";
    self.backgroundImageView.image = nil;
}

#pragma mark - ZBConfigureCellWithTheme

- (void)configureCell:(ZBThemeItemTableViewCell *)cell withTheme:(ZBTheme *)theme userData:(id)userData
{
    self.nameLabel.text = theme.name;
    self.countLabel.text = @"0";
    self.iconImageView.hidden = YES;
    self.tagsLabel.text = @"";
    self.scoreLabel.text = @"0";
    self.backgroundImageView.image = [[UIImage imageWithData:theme.image] blurredImageWithRadius:10.0 iterations:1 tintColor:[UIColor blackColor]];
}

@end
