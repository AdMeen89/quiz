//
//  ZBThemeItemTableViewCell.h
//  quiz
//
//  Created by Andrey on 23.10.15.
//  Copyright © 2015 ZBs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBConfigureCellWithTheme.h"

@interface ZBThemeItemTableViewCell : UITableViewCell <ZBConfigureCellWithTheme>

@end
